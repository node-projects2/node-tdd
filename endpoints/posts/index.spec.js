// datos para hacer post al servicio del jsonplaceholder
// {
//     userId: 1,
//     id: 1,
//     title: 'Título',
//     body: 'Cuerpo del post'
// }

const postHandlers = require('./index');
describe('Endpoints', () => {
    describe('posts', () => {
        it('should create', async () => {
            const mockUsers = [
                { id: 1 },
                { id: 2 },
                { id: 3 },
                { id: 4 },
                { id: 5 }
            ];
            const user = {
                userId: 1,
                title: 'Título',
                body: 'Cuerpo del post'
            };
            const request = {
                body: user
            };
            const responseMock = {
                status: jest.fn().mockReturnThis(),
                send: jest.fn()
            };
            const axiosMock = {
                get: jest.fn().mockResolvedValue({ data: mockUsers }),
                post: jest.fn().mockResolvedValue({ data: { id: 1001 } })
            };

            await postHandlers({ axios: axiosMock }).post(request, responseMock);
            expect(responseMock.status.mock.calls).toEqual([[201]]);
            expect(responseMock.send.mock.calls).toEqual([[{ id: 1001 }]]);

            expect(axiosMock.get.mock.calls).toEqual([
                ['https://jsonplaceholder.typicode.com/users']
            ]);
            expect(axiosMock.post.mock.calls).toEqual([
                ['https://jsonplaceholder.typicode.com/posts', user]
            ]);

        });

        it('No debe crear el usuario porque el id no existe', async () => {
            const mockUsers = [
                { id: 1 },
                { id: 2 }
            ];
            const user = {
                userId: 3,
                title: 'Título',
                body: 'Cuerpo del post'
            };
            const request = {
                body: user
            };
            const responseMock = {
                status: jest.fn().mockReturnThis(),
                send: jest.fn(),
                sendStatus: jest.fn()
            };
            const axiosMock = {
                get: jest.fn().mockResolvedValue({ data: mockUsers }),
                post: jest.fn().mockResolvedValue({ data: { id: 1001 } })
            };

            await postHandlers({ axios: axiosMock }).post(request, responseMock);

            expect(responseMock.sendStatus.mock.calls).toEqual([[400]]);

            expect(axiosMock.get.mock.calls).toEqual([
                ['https://jsonplaceholder.typicode.com/users']
            ]);

            // No llamado
            expect(axiosMock.post.mock.calls).toEqual([]);
            expect(responseMock.send.mock.calls).toEqual([]);

        });
    });
});