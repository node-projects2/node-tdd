var express = require('express');
const parser = require('body-parser');
const { posts } = require('./endpoints');
const { authenticate } = require('./middlewares');
const services = require('./services');
var app = express();

// parse application/x-www-form-urlencoded
app.use(parser.urlencoded({ extended: false }));

// parse application/json
app.use(parser.json());

const postsHandlers = posts(services);

// creamos middleware del authenticate
app.post('/', authenticate, postsHandlers.post);

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

module.exports = app;