const authenticate = require('./authenticate');

describe('Middlewares', () => {
    describe('authenticate', () => {
        it('Tiene que ser administrador (id=1)', () => {
            // authenticate recibe req, res y next

            const requestMock = {
                header: jest.fn().mockReturnValue('1')
            };
            const responseMock = {
                sendStatus: jest.fn()
            };
            const nextMock = jest.fn();

            authenticate(requestMock, responseMock, nextMock);

            expect(requestMock.header.mock.calls).toEqual([
                ['user_id']
            ]);
            // que no haya sido llamado
            expect(responseMock.sendStatus.mock.calls).toEqual([]);
            // Llamado sin parámetros.
            expect(nextMock.mock.calls).toEqual([[]]);

        });

        it('Debe fallar si el usuario no es administrador (id!=1)', () => {
            // authenticate recibe req, res y next

            const requestMock = {
                header: jest.fn().mockReturnValue('2')
            };
            const responseMock = {
                sendStatus: jest.fn()
            };
            const nextMock = jest.fn();

            authenticate(requestMock, responseMock, nextMock);

            expect(requestMock.header.mock.calls).toEqual([
                ['user_id']
            ]);
            // da error de no autorizado
            expect(responseMock.sendStatus.mock.calls).toEqual([[403]]);
            // No se ha llamado al next
            expect(nextMock.mock.calls).toEqual([]);

        });
    });
});